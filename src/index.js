import 'dotenv/config';
import express from 'express';
import debug from 'debug';
import Acl from './services/acl';
import database from './services/database';

const app = express();
const log = debug('app');
const logError = debug('app:error');
const PORT = process.env.PORT || 3000;

function start() {
  app.listen(PORT, (err) => {
    if (err) {
      log(err);
      process.exit(1);
    }

    log(`Server listening on port ${PORT}`);
  });
}

database.once('open', () => {
  const acl = Acl(database.db);

  Promise.all([
    acl.allow('guest', 'books', 'get'),
    acl.allow('user', 'books', ['get', 'put', 'post', 'update', 'delete']),
    // Test revoking - seems to do nothing
    acl.removeAllow('user', 'books', ['put', 'post', 'update', 'delete']),
    acl.addUserRoles('jake', 'user'),
    acl.allowedPermissions('jake', ['books']),
  ])
  .then(start)
  .catch(logError);
});

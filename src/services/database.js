/**
 * Database service for dependency injection.
 */

import debug from 'debug';
import mongoose from 'mongoose';

mongoose.Promise = Promise;
const log = debug('app:services:database');

const database = (() => {
  // Connect only once
  if ([1, 2].includes(mongoose.connection.readyState)) {
    return mongoose.connection;
  }

  let host = process.env.MONGO_URL;
  const opts = {
    server: {
      auto_reconnect: true, // eslint-disable-line camelcase
      reconnectTries: Number.MAX_SAFE_INTEGER,
      reconnectInterval: 2500,
    },
  };

  if (process.env.NODE_ENV === 'test') {
    host = 'mongodb://localhost:27017/express-acl-test';
  }

  mongoose.connect(host, opts)
    .catch(err => log('Database connection error', err));

  mongoose.connection.on('error', (err) => {
    log('Database error', err);
  });

  mongoose.connection.on('connected', () => {
    log(`Database connected @ ${host}`);
  });

  mongoose.connection.on('disconnected', () => {
    log('Database connection closed');
  });

  mongoose.connection.on('reconnected', () => {
    log(`Database reconnected @ ${host}`);
  });

  mongoose.connection.once('open', () => {
    log(`Database connection open @ ${host}`);
  });

  return mongoose.connection;
})();

export default database;

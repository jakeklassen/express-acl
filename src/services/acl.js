import Acl from 'acl';

export default database => new Acl(new Acl.mongodbBackend(database, 'acl_', true));
